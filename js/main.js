let number1 , result ,counter = 0 ;
let number = "" , displayCalculator = "" ;

function acButton() {
    document.getElementById("inPut").innerHTML = "" ;
    document.getElementById("outPut").innerHTML = "" ;
    displayCalculator = "" ;
    number = "" ;
    result = 0 ;
    counter=0;
}

function zeroButton() {
    number = number + "0" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function oneButton() {
    number = number + "1" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function twoButton() {
    number = number + "2" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function threeButton() {
    number = number + "3" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function fourButton() {
    number = number + "4" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function fiveButton() {
    number = number + "5" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function sixButton() {
    number = number + "6" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function sevenButton() {
    number = number + "7" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function eightButton() {
    number = number + "8" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function nineButton() {
    number = number + "9" ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function decimalPointButton() {
    number = number + "." ;
    document.getElementById("inPut").innerHTML = displayCalculator + number ;
}

function moveValue() {
    displayCalculator = displayCalculator + number ;
    number1 = Number( number ) ;
    number="";
}

let valueResiduaButton = false ;
function residualButton() {
    moveValue();
    displayCalculator = displayCalculator + "%" ;
    document.getElementById("inPut").innerHTML =displayCalculator;
    valueResiduaButton = true ;
    counter++;
}

let valueSquareRootButton=false;
function squareRootButton() {
    moveValue();
    displayCalculator = displayCalculator + "√" ;
    document.getElementById("inPut").innerHTML = displayCalculator ;
    valueSquareRootButton = true ;
    counter++;
}

let valuePowerButton = false ;
function powerButton() {
    moveValue();
    displayCalculator = displayCalculator + "^" ;
    document.getElementById("inPut").innerHTML = displayCalculator ;
    valuePowerButton = true ;
    counter++;
}

let valueDivisionButton = false ;
function divisionButton() {
    moveValue();
    displayCalculator = displayCalculator + "÷" ;
    document.getElementById("inPut").innerHTML = displayCalculator ;
    valueDivisionButton = true ;
    counter++;
}

let valueMultiplicationButton = false ;
function multiplicationButton() {
    moveValue();
    displayCalculator = displayCalculator + "×" ;
    document.getElementById("inPut").innerHTML = displayCalculator ;
    valueMultiplicationButton = true;
    counter++;
}

let valueSubtractionButton = false ;
function subtractionButton(){
    moveValue();
    displayCalculator= displayCalculator + "-" ;
    document.getElementById("inPut").innerHTML = displayCalculator ;
    valueSubtractionButton = true;
    counter++;
}

let valueAdditionButton=false;
function additionButton() {
   moveValue();
    displayCalculator = displayCalculator + "+" ;
    document.getElementById("inPut").innerHTML = displayCalculator ;
    valueAdditionButton = true ;
    counter++;
}

function equalsButton() {
  if(counter<2){
    if( valueSquareRootButton ) {
        result = Math.sqrt ( number );
        valueSquareRootButton = false ;
    }
    if( valueResiduaButton ) {
        result = number1 % number ;
        valueResiduaButton = false ;
    }

    if( valuePowerButton ) {
        result = Math.pow ( number1 , number ) ;
        valuePowerButton = false ;
    }

    if( valueDivisionButton ) {
        result = number1 / number ;
        valueDivisionButton = false ;
    }

    if( valueMultiplicationButton ) {
        result = number1 * number ;
        valueMultiplicationButton = false ;
    }

    if( valueSubtractionButton ) {
        result = number1 - number ;
        valueSubtractionButton = false ;
    }

    if( valueAdditionButton ) {
        result = number1 + Number(number);
        valueAdditionButton=false;
    }
    document.getElementById("outPut").innerHTML = result ;
  } else {
      document.getElementById("inPut").innerHTML = 'Please select only one operator' ;
  }
}






